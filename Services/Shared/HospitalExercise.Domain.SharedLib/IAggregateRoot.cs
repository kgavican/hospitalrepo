﻿namespace HospitalExercise.Domain.SharedLib
{
   /// <summary>
    /// IAggregateRoot. That interface is an empty interface, sometimes called a marker interface, 
    /// that is used just to indicate that this entity class is also an aggregate root.
   /// </summary>
    public interface IAggregateRoot { }

}
