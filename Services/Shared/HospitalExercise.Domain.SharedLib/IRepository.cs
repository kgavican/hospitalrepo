﻿namespace HospitalExercise.Domain.SharedLib
{
    public interface IRepository<T> where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
