﻿using System.Threading;
using MediatR;
using System.Threading.Tasks;
using HospitalCommand.Domain.Aggregates.HosptialAggregate;
using HospitalCommand.Infrastructure.EntityConfigurations;
using HospitalExercise.Domain.SharedLib;

using Microsoft.EntityFrameworkCore;

namespace HospitalCommand.Infrastructure
{
	public class HospitalCommandContext : DbContext, IUnitOfWork
    {
		private IMediator _mediator;

		public DbSet<Hospital> Hospitals { get; set; }

      
		public HospitalCommandContext(DbContextOptions options) : base(options)
		{
		}

		public HospitalCommandContext(DbContextOptions options, IMediator mediator) : base(options) => _mediator = mediator;

		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			modelBuilder.ApplyConfiguration(new HospitalEntityTypeConfiguration());
        }

		public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			await _mediator.DispatchDomainEventsAsync(this);

            var result = await base.SaveChangesAsync();
                     
            return true;
		}

	}
}
