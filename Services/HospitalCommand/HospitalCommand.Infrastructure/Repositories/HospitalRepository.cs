﻿using HospitalCommand.Domain.Aggregates.HosptialAggregate;
using HospitalExercise.Domain.SharedLib;

namespace HospitalCommand.Infrastructure.Repositories
{
	public class HospitalRepository : IHospitalRepository
    {
		private readonly HospitalCommandContext _context;

		public IUnitOfWork UnitOfWork {
			get
            {
                return _context;
            }
		}
      
		public HospitalRepository(HospitalCommandContext ctx){
			_context = ctx;
		}

		public Hospital CreateHospital(Hospital hospital)
		{
			return _context.Hospitals.Add(hospital).Entity;
		}
	}
}
