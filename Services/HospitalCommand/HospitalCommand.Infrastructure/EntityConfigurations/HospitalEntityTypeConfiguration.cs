﻿using HospitalCommand.Domain.Aggregates.HosptialAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HospitalCommand.Infrastructure.EntityConfigurations
{
	public class HospitalEntityTypeConfiguration : IEntityTypeConfiguration<Hospital>
    {
		public void Configure(EntityTypeBuilder<Hospital> hospitalConfig)
        {         
			// ToDo: Map our domain model to entity table
        }
    }
}
