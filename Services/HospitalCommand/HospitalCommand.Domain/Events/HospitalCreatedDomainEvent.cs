﻿using HospitalCommand.Domain.Aggregates.HosptialAggregate;
using HospitalExercise.Domain.SharedLib;
using MediatR;

namespace HospitalCommand.Domain.Events
{
	public class HospitalCreatedDomainEvent : INotification
    {
		private Hospital _hospital;
              
		public HospitalCreatedDomainEvent(Hospital hospital){
			_hospital = hospital;         
		}
    }
}
