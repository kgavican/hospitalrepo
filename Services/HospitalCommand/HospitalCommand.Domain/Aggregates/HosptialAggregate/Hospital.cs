﻿using System;
using HospitalCommand.Domain.Events;
using HospitalExercise.Domain.SharedLib;

namespace HospitalCommand.Domain.Aggregates.HosptialAggregate
{
	public class Hospital : Entity, IAggregateRoot
    {
		private string _name;
      
		public Address Address { get; private set; }
        
		private DateTime _created;

		public Hospital(string name, Address address)
		{
			// Some example validation within domain constructor

			_name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));

			Address = address ?? throw new ArgumentNullException(nameof(address));

			_created = DateTime.UtcNow;

			// Add to the current domain events (in our entity base) to be dipatched when the commit happens 
			AddDomainEvent(new HospitalCreatedDomainEvent(this));
		}   

		public string GetHospitalName()
        {
			return _name;
        }
	};
}
