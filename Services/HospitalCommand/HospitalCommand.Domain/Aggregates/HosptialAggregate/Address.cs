﻿using System;
using System.Collections.Generic;
using HospitalExercise.Domain.SharedLib;

namespace HospitalCommand.Domain.Aggregates.HosptialAggregate
{
	public class Address : ValueObject
    {
		public string AddressLine1 { get; }
		public string AddressLine2 { get; }
		public string County { get; }
		public string City { get; }
		public string Country { get; }
		public string Postcode { get; }

        private Address() { }

		public Address(string addressLine1, string addressLine2, string county, string city, string country, string postcode)
        {         
			// Some example validation within domain constructor
			AddressLine1 = !string.IsNullOrWhiteSpace(addressLine1) ? addressLine1 : throw new ArgumentNullException(nameof(addressLine1));
			AddressLine2 = addressLine2;
			County = !string.IsNullOrWhiteSpace(county) ? county : throw new ArgumentNullException(nameof(county));
			City = city;
            Country = country;
			Postcode = !string.IsNullOrWhiteSpace(postcode) ? postcode : throw new ArgumentNullException(nameof(postcode));
        }
              
		protected override IEnumerable<object> GetAtomicValues()
		{
			yield return AddressLine1;
			yield return AddressLine2;
			yield return County;
			yield return City;
			yield return Country;
			yield return Postcode;
		}
	}
}
