﻿using HospitalExercise.Domain.SharedLib;

namespace HospitalCommand.Domain.Aggregates.HosptialAggregate
{
	public interface IHospitalRepository : IRepository<Hospital>
    {
	    Hospital CreateHospital(Hospital hospital);
    }
}
