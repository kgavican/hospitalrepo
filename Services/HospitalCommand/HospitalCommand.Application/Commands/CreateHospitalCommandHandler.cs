﻿using System.Threading.Tasks;
using HospitalCommand.Domain.Aggregates.HosptialAggregate;
using NServiceBus;

namespace HospitalCommand.Application.Commands
{
	public class CreateHospitalCommandHandler : IHandleMessages<CreateHospitalCommand>
    {
		private readonly IHospitalRepository _hospitalRepository;
      
		public CreateHospitalCommandHandler(IHospitalRepository hospitalRepository)
        {
			_hospitalRepository = hospitalRepository;
        }
        
      
		public async Task Handle(CreateHospitalCommand message, IMessageHandlerContext context)
		{
			// Handle our command for CreateHospital by creating and saving to DB
            // Validation will happen in the domain layer

            var address = new Address(message.AddressLine1, message.AddressLine2, message.County, 
                                      message.City, message.Country, message.Postcode);

            var hospital = new Hospital(message.Name, address);

            // if we make it this far we create
            _hospitalRepository.CreateHospital(hospital);


            // Shared libary Base classes are used in the SaveChanges for dipatching any Domain events for the entity Save
            await _hospitalRepository.UnitOfWork
                .SaveEntitiesAsync();

		}
	}
}
