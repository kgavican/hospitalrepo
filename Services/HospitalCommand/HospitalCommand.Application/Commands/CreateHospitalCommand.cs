﻿using System.Runtime.Serialization;
using NServiceBus;

namespace HospitalCommand.Application.Commands
{
    // Serializable message for CreateCommand receive

	[DataContract]
	public class CreateHospitalCommand : ICommand
    {
        [DataMember]
        public string Name { get; private set; }

        [DataMember]
        public string AddressLine1 { get; private set; }

        [DataMember]
        public string AddressLine2 { get; private set; }

        [DataMember]
        public string County { get; private set; }

        [DataMember]
        public string City { get; private set; }

        [DataMember]
        public string Country { get; private set; }

        [DataMember]
        public string Postcode { get; private set; }

		public CreateHospitalCommand()
        {

        }

		public CreateHospitalCommand(string name, string addressLine1, string addressLine2,
                                  string county, string city, string country, string postcode)
        {
            Name = name;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            County = county;
            City = city;
            Country = country;
            Postcode = postcode;
        }
    }
}
