﻿using System;
using HospitalCommand.Domain.Aggregates.HosptialAggregate;
using Xunit;

namespace UnitTests.HospitalCommandService.Domain
{
	public class HospitalAggregateTests
    {
		[Fact]
        public void HosptialIsCreatedSuccessfully()
        {
			// arrange
			var name = "Some Hospital";
			var address = new Address("Address line 1","Address line 2", "County", "City","Country","Postcode");

			// act 
			var hospital = new Hospital(name, address);

			// assert
			Assert.NotNull(hospital);
        }

		[Fact]
        public void HospitalCreateMustHaveName()
        {
			// arrange
			var name = " ";
			var address = new Address("Address line 1", "Address line 2", "County", "City", "Country", "Postcode");

			// act/assert
			var ex = Assert.Throws<ArgumentNullException>(() => new Hospital(name, address));

            // assert detail
			Assert.Equal(ex.ParamName, "name");
        }

		[Fact]
		public void HospitalCreateMustHaveAddress()
        {
			// arrange
			var name = "Some Hospital";
			Address address = null;
            
			// act/assert
			var ex = Assert.Throws<ArgumentNullException>(() => new Hospital(name, address));

			// assert detail
			Assert.Equal(ex.ParamName, "address");
        }

		[Fact]
        public void CreateNewHospitalAddsDomainEvent()
        {
			// arrange
            var name = "Some Hospital";
            var address = new Address("Address line 1", "Address line 2", "County", "City", "Country", "Postcode");

            // act 
            var hospital = new Hospital(name, address);

			// assert
			Assert.Equal(hospital.DomainEvents.Count, 1);

        }
    }
}
