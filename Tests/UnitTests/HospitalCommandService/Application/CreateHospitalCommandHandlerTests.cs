﻿using System;
using System.Threading.Tasks;
using HospitalCommand.Application.Commands;
using HospitalCommand.Domain.Aggregates.HosptialAggregate;
using NServiceBus.Testing;
using NSubstitute;
using Xunit;

namespace UnitTests.HospitalCommandService.Application
{
	public class CreateHospitalCommandHandlerTests
    {      
		[Fact]
		public async Task CreateHospitalValidCommandCausesSaveToDatabase()
        {         
			var orderRepositoryMock = Substitute.For<IHospitalRepository>();

			// Arrange 
			var handler = new CreateHospitalCommandHandler(orderRepositoryMock);
			var context = new TestableMessageHandlerContext();

			var receivedMessage = new CreateHospitalCommand("St James","Address line 1", "Address line 2", "County", "City", "Country", "Postcode");
            
  			// Act 
			await handler.Handle(receivedMessage, context);

			// Assert
			orderRepositoryMock.Received().CreateHospital(Arg.Is<Hospital>(x => string.Equals(x.GetHospitalName(), "St James")));
        }

		[Fact]
		public async Task CreateHospitalInValidCommandDoesntCauseSaveToDatabase()
        {
			var orderRepositoryMock = Substitute.For<IHospitalRepository>();

            // Arrange 
            var handler = new CreateHospitalCommandHandler(orderRepositoryMock);
            var context = new TestableMessageHandlerContext();

			var receivedMessage = new CreateHospitalCommand(string.Empty, "Address line 1", "Address line 2", "County", "City", "Country", "Postcode");

			// Act 
			Func<Task> testCode = async () => await handler.Handle(receivedMessage, context);
			await Assert.ThrowsAsync<ArgumentNullException>(testCode);
         
            // Assert
			orderRepositoryMock.DidNotReceive().CreateHospital(Arg.Any<Hospital>());
        }
       
        /*
		[Fact]
	    public Task CreateHospitalInValidCommandRaisesDomainEvent()
		{
			Need to test that the domain event is fired upon the save changes
		}
        */
    }
}
